﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSignalRLoic.Hubs
{
    public class ChatHub : Hub
    {

        //public void Hello()
        //{
        //    Clients.Others.SendAsync("Test");
        //}

        static List<string> context = new List<string>();

        public void GetAll()
        {
            Clients.Caller.SendAsync("Refresh", context);
        }

        public void Post(string value)
        {
            context.Add(value);
            Clients.All.SendAsync("Refresh", context);
        }
    }
}
